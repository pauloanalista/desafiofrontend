angular.module("shotApp").filter('removeTagHtml', function () {
      return function (text) {
          return String(text).replace(/<[^>]+>/gm, '');
      };
  }
);