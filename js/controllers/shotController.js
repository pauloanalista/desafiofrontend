angular.module("shotApp").controller("shotController", function ($scope, shotAPI) {
    //Define titulo e exibe o aguarde
	$scope.tituloDoApp = "Lista de Shots";
    $scope.showLoading = true;
	
	//Carrega a lista de Shots
    var loadShotCollection = function () {
        //Oculta as telas de Lista de Shots e Detalhe de Shot
		$scope.showDetail = false;
		$scope.showShotCollection = false;
		
		//Exibe o aguarde
		$scope.showLoading = true;
		
		//Realiza uma chamada assincrona para obter uma lista de Shots
		shotAPI.getShotCollection().success(function (data) {
			 //Exibe a tela de lista de Shots e oculta o aguarde
			 $scope.showShotCollection = true;
			 $scope.showLoading = false;
			 
			 //Carrega a tela com os itens que vieram da API
			 $scope.shotCollection = data;
         }).error(function(data, status, headers, config) {
			//Oculta o aguarde e exibe uma mensagem de falha no processamento
			$scope.showLoading = false;
			alert('Operação falhou, tente novamente mais tarde!');
		});
    };
	
	//Carrega o detalhe do Shot selecionado
    $scope.detalharShot = function (id) {
		//Oculta as telas de Lista de Shots e Detalhe de Shot
		$scope.showDetail = false;
		$scope.showShotCollection = false;
		
		//Exibe o aguarde e define o titulo da tela de detalhe
		$scope.showLoading = true;
		$scope.tituloDoApp = "Detalhe do Shot";
		
		//Realiza uma chamada assincrona para obter o detalhe do Shot selecionado
        shotAPI.getShot(id).success(function (data) {
			//Oculta aguarde e exibe a tela de detalhe do Shot selecionado
			$scope.showLoading = false;
			$scope.showDetail = true;
			
			//Carrega a tela com os dados do Shot selecionado
			$scope.shotSelecionado =  data;
        
		}).error(function(data, status, headers, config) {
			//Oculta o aguarde e exibe uma mensagem de falha no processamento
			$scope.showLoading = false;
			alert('Operação falhou, tente novamente mais tarde!');
		});
    };

    //Volta para tela de Lista de Shots sem a necessidade de uma chamada assincrona
	$scope.voltarListaDeShot = function () {
		$scope.showLoading = true;
		$scope.showShotCollection = true;
		$scope.showDetail = false;
        $scope.tituloDoApp = "Lista de Shots";
		$scope.showLoading = false;
    };

    
    

    loadShotCollection();
});

angular.module("shotApp").filter('removeTagHtml', function () {
      return function (text) {
          return String(text).replace(/<[^>]+>/gm, '');
      };
  }
);