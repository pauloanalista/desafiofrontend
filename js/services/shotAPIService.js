angular.module("shotApp").service("shotAPI", function ($http) {
    this.token = "88b060a97d22603df099f51011f2450cc991c98afe1ebd83ba4ac7f1f6f5c857";
    this.getShotCollection = function () {
        return $http.get("https://api.dribbble.com/v1/shots?sort=viewsuser&access_token=" + this.token);
    };

    this.getShot = function (id) {
        return $http.get("https://api.dribbble.com/v1/shots/" + id + "?access_token=" + this.token);
    };
});